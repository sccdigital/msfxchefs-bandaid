let formData = {
	requiredTextInputs: [
		{
			name: "firstName",
			text: "",
			valid: false
		},
		{
			name: "lastName",
			text: "",
			valid: false
		},
		{
			name: "email",
			text: "",
			valid: false
		},
		{
			name: "accountName",
			text: "",
			valid: false
		},
		{
			name: "title",
			text: "",
			valid: false
		},
		{
			name: "addressOne",
			text: "",
			valid: false
		},
		{
			name: "city",
			text: "",
			valid: false
		},
		{
			name: "zipCode",
			text: "",
			valid: false
		},
		{
			name: "distributor",
			text: "",
			valid: false
		}
	],
	requiredSelectInputs: [
		{
			name: "state",
			valid: false
		},
		{
			name: "segment",
			valid: false
		},
		{
			name: "numberOfUnits",
			valid: false
		},
		{
			name: "fAndBPurchases",
			valid: false
		},
		{
			name: "contractManagedGPO",
			valid: false
		},
		{
			name: "typeOfOperation",
			valid: false
		},
		{
			name: "termsAndPrivacy",
			valid: false
		}
	],
	buttonState: {
		disabled: true
	},
	pardotSubmission: {
		success: false
	}
}

window.addEventListener("DOMContentLoaded", function() {
    // Validate Text Inputs on Keyup
    const requiredTextFields = document.querySelectorAll(".required");
    for (let i = 0; i < requiredTextFields.length; i++) {
    	requiredTextFields[i].addEventListener("keyup", function(e) {
    		let inputName = getInputEntered(e.target);
			const inputs = formData.requiredTextInputs;
			let input = inputs.find(function(input) {
				return input.name === inputName;
			});
			input.text = e.target.value;

			if (inputName != "email" && inputName != "zipCode" && input.text.length >= 1 && input.text.length <= 255) {
				manageValidEntry(input, inputName);
			} else if (inputName === "email" && validateEmail(input.text)) {
				manageValidEntry(input, inputName);
			} else if (inputName === "zipCode" && validateZipCode(input.text)) {
				manageValidEntry(input, inputName)
			} else {
				manageInvalidEntry(input, inputName);
			}
			
			manageButtonState();
    	})
    }
  	

   	// Validate Select + Checkbox Validation on Change
   	const requiredSelectFields = document.querySelectorAll(".required-select");
   	for (let i = 0; i < requiredSelectFields.length; i++) {
   		requiredSelectFields[i].addEventListener("change", function(e) {
   			let inputName = getInputEntered(e.target);
			const inputs = formData.requiredSelectInputs;
			let input = inputs.find(function(input) {
				return input.name === inputName;
			});
			if (e.target.type === "select-one") {
				manageValidEntry(input, inputName);
			} else if (e.target.checked) {
				manageValidEntry(input, inputName);
			} else {
				manageInvalidEntry(input, inputName);
			}
			
			manageButtonState();
   		})
   	}

	function getInputEntered(input) {
		return input.name
	}

	function validateEmail(email) {
	    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}

	function validateZipCode(zipCode) {
	   return /^\d{5}(-\d{4})?$/.test(zipCode);
	}

	function manageValidEntry (input, inputName) {
		input.valid = true;
		// Mindtree has styles built in for #zipCodeError
		if (inputName === "zipCode") {
			document.querySelector("#" + inputName + "Err").classList.remove("show");
		} else {
			document.querySelector("#" + inputName + "Error").classList.remove("show");
		}
	}

	function manageInvalidEntry(input, inputName) {
		input.valid = false;
		// Mindtree has styles built in for #zipCodeError
		if (inputName === "zipCode") {
			document.querySelector("#" + inputName + "Err").classList.add("show");
		} else {
			document.querySelector("#" + inputName + "Error").classList.add("show");
		}
	}

	function areValid(item) {
		return item.valid === true;
	}

	function manageButtonState() {
		if (formData.requiredTextInputs.every(areValid) && formData.requiredSelectInputs.every(areValid)) {
			formData.buttonState.disabled = false;
			document.querySelector("#getRebateBtn").disabled = false;
		} else {
			formData.buttonState.disabled = true;
			document.querySelector("#getRebateBtn").disabled = true;
		}
	}

	// Begin Form Submission Handling
	function getUrl(url) {
		return url
	}

	function isFormSubmitted() {
		return url.includes("?success=true");
	}

	function manageSuccessfulSubmission() {
		formData.pardotSubmission.success = true;
		document.body.classList.add("submitted");
		document.querySelector(".form-header-container").remove();
		document.querySelector(".form-content").remove();
		document.querySelector(".submission-thank-you-hide").classList.add("submission-thank-you");
		document.querySelector(".submission-thank-you").classList.remove("submission-thank-you-hide");
		document.querySelector(".submission-thank-you").innerText = "Thanks for signing up! Check your email inbox for your exclusive rebate.";
	}

	let url = getUrl(window.location.href);
	if (isFormSubmitted(url)) {
		manageSuccessfulSubmission();
	}
	// End Form Submission Handling
});