let formData = {
	requiredTextInputs: [
		{
			name: "firstName",
			text: "",
			valid: false
		},
		{
			name: "lastName",
			text: "",
			valid: false
		},
		{
			name: "email",
			text: "",
			valid: false
		},
		{
			name: "accountName",
			text: "",
			valid: false
		},
		{
			name: "title",
			text: "",
			valid: false
		},
		{
			name: "addressOne",
			text: "",
			valid: false
		},
		{
			name: "city",
			text: "",
			valid: false
		},
		{
			name: "zipCode",
			text: "",
			valid: false
		},
		{
			name: "distributor",
			text: "",
			valid: false
		}
	],
	requiredSelectInputs: [
		{
			name: "state",
			valid: false
		},
		{
			name: "segment",
			valid: false
		},
		{
			name: "numberOfUnits",
			valid: false
		},
		{
			name: "fAndBPurchases",
			valid: false
		},
		{
			name: "contractManagedGPO",
			valid: false
		},
		{
			name: "typeOfOperation",
			valid: false
		},
		{
			name: "termsAndPrivacy",
			valid: false
		}
	],
	buttonState: {
		disabled: true
	},
	pardotSubmission: {
		success: false
	}
}

window.addEventListener('DOMContentLoaded', (event) => {
    // Validate Text Inputs on Keyup
    document.querySelectorAll(".required").forEach(requiredField => {
		requiredField.addEventListener("keyup", e => {
			let inputName = getInputEntered(e.target);
			const inputs = formData.requiredTextInputs;
			let input = inputs.find(input => input.name === inputName);
			input.text = e.target.value;

			if (inputName != "email" && inputName != "zipCode" && input.text.length >= 1 && input.text.length <= 255) {
				manageValidEntry(input, inputName);
			} else if (inputName === "email" && validateEmail(input.text)) {
				manageValidEntry(input, inputName);
			} else if (inputName === "zipCode" && validateZipCode(input.text)) {
				manageValidEntry(input, inputName)
			} else {
				manageInvalidEntry(input, inputName);
			}
			
			manageButtonState();
		})
	})

   	// Validate Select + Checkbox Validation on Change
	document.querySelectorAll(".required-select").forEach(requiredSelect => {
		requiredSelect.addEventListener("change", e => {
			let inputName = getInputEntered(e.target);
			const inputs = formData.requiredSelectInputs;
			let input = inputs.find(input => input.name === inputName);
			if (e.target.type === "select-one") {
				manageValidEntry(input, inputName);
			} else if (e.target.checked) {
				manageValidEntry(input, inputName);
			} else {
				manageInvalidEntry(input, inputName);
			}
			
			manageButtonState();
		})
	})

	const getInputEntered = (input) => {
		return input.name
	}

	const validateEmail = (email) => {
	    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}

	const validateZipCode = (zipCode) => {
	   return /^\d{5}(-\d{4})?$/.test(zipCode);
	}

	const manageValidEntry = (input, inputName) => {
		input.valid = true;
		document.querySelector("#" + inputName + "Error").classList.remove("show");
	}

	const manageInvalidEntry = (input, inputName) => {
		input.valid = false;
		document.querySelector("#" + inputName + "Error").classList.add("show");
	}

	const areValid = (item) => item.valid === true;

	const manageButtonState = () => {
		if (formData.requiredTextInputs.every(areValid) && formData.requiredSelectInputs.every(areValid)) {
			formData.buttonState.disabled = false;
			document.querySelector("#getRebateBtn").disabled = false;
		} else {
			formData.buttonState.disabled = true;
			document.querySelector("#getRebateBtn").disabled = true;
		}
	}

	// Begin Form Submission Handling
	const getUrl = (url) => {
		return url
	}

	const isFormSubmitted = (url) => {
		return url.includes("?success=true");
	}

	const manageSuccessfulSubmission = () => {
		formData.pardotSubmission.success = true;
		document.body.classList.add("submitted");
		document.querySelector(".form-header-container").remove();
		document.querySelector(".form-content").remove();
		document.querySelector(".submission-thank-you-hide").classList.add("submission-thank-you");
		document.querySelector(".submission-thank-you").classList.remove("submission-thank-you-hide");
		document.querySelector(".submission-thank-you").innerText = "Thanks for signing up! Check your email inbox for your exclusive rebate.";
	}

	let url = getUrl(window.location.href);
	if (isFormSubmitted(url)) {
		manageSuccessfulSubmission();
	}
	// End Form Submission Handling
});